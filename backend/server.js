const express = require('express')
const cookieParser = require('cookie-parser');
const cors = require('cors')

const bibleService = require('./services/bible.service')
const app = express()
const port = 3030

// Express App Configuration
// We ask Express.js to serve static files from the 'public' folder
app.use(express.static('public'))
app.use(cookieParser())
if (process.env.NODE_ENV === 'production') {
    app.use(express.static(path.resolve(__dirname, 'public')))
} else {
    const corsOptions = {
        origin: ['http://127.0.0.1:8080', 'http://localhost:8080', 'http://127.0.0.1:3000', 'http://localhost:3000'],
        credentials: true
    }
    app.use(cors(corsOptions))
}


app.get('/api', async (req, res) => {
    const chapter = await bibleService.query(req.query)
    
    res.send(chapter)
})

app.listen(port, () => {
    console.log(`My app listening at http://localhost:${port}`)
})