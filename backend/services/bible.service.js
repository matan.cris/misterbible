const torah = require('../data/torahTeamimDB.json')

module.exports = {
    query,

}

function query({ book, chapter }) {
    const currChapter = torah[book].chapters[+chapter]
    // if (gCurrDisplay.term) {
    //     currChapter.verses = currChapter.verses.filter(verse => getClearTxt(verse.txt).includes(gCurrDisplay.term))
    // }

    return Promise.resolve(currChapter)
}

